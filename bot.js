// ==UserScript==
// @name         TFS Complete PR Bot

// @version      0.2.4
// @description  Save yourself!
// @author       Chris
// @match        http://tfssrvr01:8080/tfs/DefaultCollection/_git/INX-Git/pullrequest/*
// @grant        none
// @updateURL    https://gitlab.com/fronogy/tfs-complete-pr-bot/raw/master/bot.js
// @downloadURL  https://gitlab.com/fronogy/tfs-complete-pr-bot/raw/master/bot.js
// ==/UserScript==

(function () {
    'use strict';

    var bot = {
        pollTime: 20 * 1000,
        pollTimer: null,
        started: false,
        
        init: function () {
            this.populateStatusPanelProperties();
            this.generateBotPanel();
            this.populateBotPanelProperties();
			
			this.pullRequestName = $('.vc-pullrequest-title-link').text();
			
            if (this.isCompleted) {
                this.handleCompleted();
            } else {
				this.handleAutoStart();
                this.bindBotButton();
            }
        },
        populateStatusPanelProperties: function () {
            this.$statusPanel = $('.vc-pullrequest-view-details-status-panel');
            this.isApproved = this.$statusPanel.find('.vc-pullrequest-view-details-action-text:contains("Reviewers approved"):first').is(':visible');
            this.isCompleted = this.$statusPanel.find('.vc-pullrequest-view-details-status-actions button:contains("Delete source branch")').is(':visible');
            this.buildSucceeded = this.$statusPanel.find('.vc-pullrequest-view-details-action-text:contains("Build succeeded"):first').is(':visible');
            this.buildExpired = this.$statusPanel.find('.vc-pullrequest-view-details-action-text:contains("Build expired"):first').is(':visible');
            this.$completeButton = this.$statusPanel.find('.vc-pullrequest-view-details-status-actions button:contains("Complete pull request")');
            this.completeConfirmButtonSelector = '.ui-dialog-buttonpane #ok';

            this.$rebuildLink = this.$statusPanel.find('.vc-pullrequest-view-details-item-status a:contains("rebuild"):visible');
            this.rebuildLinkVisible = this.$rebuildLink.length > 0;
            this.$reevaluateLink = this.$statusPanel.find('.vc-pullrequest-view-details-action-text:contains("re-evaluate"):visible');
            this.reevaluateLinkVisible = this.$reevaluateLink.length > 0;
        },
        populateBotPanelProperties: function () {
            this.$botPanel = $('.bot-panel');
            this.$botButton = this.$botPanel.find('.bot-button');
            this.$status = this.$botPanel.find('.status');
            this.$statusRunningIcon = this.$botPanel.find('.status-progress');
        },
        generateBotPanel: function () {
            var $panel =
                '<div class="bot-panel" style="margin: 10px;">' +
                '<div class="vc-pullrequest-view-details-status">Complete Bot</div>' +
                '<div style="margin: 5px;">Status: <span class="status">Stopped</span></div>' +
                '<div class="bowtie-style">' +
                '<button type="button" class="add-panel-button btn-cta bot-button" style="margin: 5px;">Run</button>' +
                '</div>' +
                '</div>';

            this.$statusPanel.after($panel);
        },
        bindBotButton: function () {
            var self = this;

            this.$botButton.click(function () {
                if (self.started) {
                    self.stopBot();
                } else {
                    self.startBot();
                }
            });
        },
        startBot: function () {
            var self = this;
			
			function runBot() {
                self.populateStatusPanelProperties();
                console.log("interval");

                if (self.isApproved) {
                    if (self.buildSucceeded) {
                        console.log("buildSucceeded");
                        self.completePullRequest();
                    } else if (self.rebuildLinkVisible) {
                        console.log("rebuild click");
                        self.$rebuildLink.click();
					} else if(self.reevaluateLinkVisible) {
						console.log("re-evaluate click");
                        self.$reevaluateLink.click();
                    } else if(self.buildExpired) {
						console.log("expired");
						self.reloadAndQueueAutoStart();
					}
                }
			}

			runBot();
			
            this.pollTimer = setInterval(runBot, this.pollTime);

            this.updateState();
        },
        stopBot: function () {
            clearInterval(this.pollTimer);

            this.updateState();
        },
        updateState: function () {
            if (this.started) {
                this.$botButton.addClass('btn-cta').html('Run');
                this.$status.html('Stopped');
                this.$statusRunningIcon.hide();
                this.started = false;
            } else {
                this.$botButton.removeClass('btn-cta').html('Stop');
				this.$status.show().html('🏃 Running');
                this.$statusRunningIcon.show();
                this.started = true;
            }
        },
        handleCompleted: function () {
            this.$botButton.removeClass('btn-cta').prop('disabled', 'disabled').html('Duude, I\'m done...');
            this.$status.html('¯\\_(ツ)_/¯');
        },
        completePullRequest: function () {
            console.log("complete click");

            this.$completeButton.click();

            var self = this;
            setTimeout(function () {
                $(self.completeConfirmButtonSelector).click();
            }, 1000);

            this.stopBot();
            this.handleCompleted();
        },
		handleAutoStart: function() {
			if(sessionStorage.getItem(this.pullRequestName)) {
				console.log("auto started");
				sessionStorage.removeItem(this.pullRequestName);
				this.startBot();
			}
		},
		reloadAndQueueAutoStart: function() {
			sessionStorage.setItem(this.pullRequestName, true);
			window.location.reload();
		}
    };

    $(function () {
        setTimeout(function () {
            bot.init();
        }, 2000);
    });
})();